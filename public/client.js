var module = angular.module('myapp', ['ngResource']);

module.factory('Entry', function ($resource) {
        return $resource('/:id');
    })
    .controller('Controller', function ($scope, $http, Entry) {
        var url = function () {
            return {id: $scope.url || ''};
        };
        $scope.list = function () {
            $scope.url = 'list'
            $scope.items = Entry.query(url());
        };
        $scope.ckeck = function () {
            $http({method: 'GET', url: '/check'}).
            then(function success(response) {
                $scope.label = response.data;
            });
        };
        $scope.file = function () {
            var input = document.getElementById("filename");
            var file = input.value.split("\\");
            var fileName = file[file.length - 1];
            $http({
                url: '/file',
                method: "POST",
                data: fileName
            }).then(function (response) {
                    $scope.isError = true;
                },
                function (response) {
                    $scope.isError = false;
                });
        };
    });
