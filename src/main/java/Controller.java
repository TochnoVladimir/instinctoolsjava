import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.*;

@RestController
@RequestMapping(value = "/")
public class Controller {
    String fileName;

    @RequestMapping(value = "file", produces = "text/html", method = RequestMethod.POST)
    String readFile(@RequestBody String file) {
        System.out.println("file");
        fileName = file;
        try {
            read(file);
            return "файл найден";
        } catch (FileNotFoundException e) {
            return "файл не найден";
        }
    }

    @RequestMapping(value = "list")
    List readList() {
        try {
            return countWord(read(fileName)).subList(0, 10);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "check", produces = "text/html")
    String check() {
        try {
            return checkBkt(read(fileName));
        } catch (FileNotFoundException e) {
            return "ошибка чтения файла";
        }
    }

    public List countWord(String text) {
        //Забираем текст и разбиваем его на слова
        HashMap<String, Integer> hashMap = new HashMap<>();
        StringTokenizer tokenizer = new StringTokenizer(text, " ,.:;!?({[)}]\t\n");
        while (tokenizer.hasMoreTokens()) {
            final String word = tokenizer.nextToken();
            Integer count = hashMap.get(word);
            if (count != null)
                hashMap.put(word, count + 1);
            else
                hashMap.put(word, 1);
        }
        try {
            tokenizer = new StringTokenizer(read("exception.txt"), "\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //Забираем текст с предлогами и удаляем их из первоначального текста
        while (tokenizer.hasMoreTokens()) {
            hashMap.remove(tokenizer.nextToken());
        }
        List<Map.Entry<String, Integer>> list = new ArrayList<>();
        //Переводим hashMap в список элементов ключ-значение
        hashMap.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue()
                .reversed()).forEach(list::add);
        return list;
    }

    public String read(String fileName) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        File file = new File(fileName);
        try {
            BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            try {
                String s;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    public String checkBkt(String text) {
        //Порядок скобок обязателен!
        String openBkt = "({[";
        String closeBkt = ")}]";
        String strBkt = text.replaceAll("[^(\\(\\)\\{\\}\\[\\])]", "");
        StringBuffer stack = new StringBuffer(" ");
        Boolean flag = true;
        for (int i = 0; i < strBkt.length(); i++) {
            //При открытие скобки просто добавляем её в стек
            if (openBkt.indexOf(strBkt.charAt(i)) >= 0) {
                stack.append(strBkt.charAt(i));
            }
            if (closeBkt.indexOf(strBkt.charAt(i)) >= 0) {
                //Если скобка закрывающая то проверяем предыдущую скобку на совпадение, если не совпадают то прерываем перебор
                if (closeBkt.indexOf(strBkt.charAt(i)) != openBkt.indexOf(stack.charAt(stack.length() - 1))) {
                    flag = false;
                    break;
                } else {
                    //Если скобки совппали, то удаляем их из стека
                    stack.deleteCharAt(stack.length() - 1);
                }
            }
        }
        //Если все скобки совпадают и длинна стека равна первоначальному пробелу, то возвращаем "correct"
        if (flag && (stack.length() == 1)) {
            return strBkt + " correct";
        } else return strBkt + " incorrect";
    }
}